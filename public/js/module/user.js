


var user = new function () {

    this.is_edit = 0;
    this.user_id;
    this.init = function () {


        user.saveData();
        user.createList();
    };


    this.showModal = function (is_edit = null, user_id = null) { // user.showModal();

        // for edit or update 
        if (is_edit == 1) {
            user.is_edit = 1;
            
            
            $("#addUser").attr("action", 'user_update');
            user.user_id = user_id;
            $('#img_div').removeClass('hide');
            user.putDataForEdit();
            $('#m_title').html('Edit User');
        } else { // for add 
            $('#img_div').addClass('hide');
            user.is_edit = 0;
            $("#addUser").attr("action", 'user_add');
            user.resetValues();
            $('#id').val(user.user_id);
            $('#m_title').html('Add New User');
            $('#addModal').modal('show');
    }

    };

    this.putDataForEdit = function () {

        $.get('user/' + user.user_id + '/edit', function (data) {
            
            
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#email').val(data.email);
            $('#phone_number').val(data.phone_number);
            $('#txtAddress').val(data.address);
            $('#city').val(data.city);
            $('#already_uploded_path').val(data.path);
            
            if(data.path){
                var path_ = data.path.replace('public', '/storage');
                $('#img_').attr('src',path_);
            }else{
                $('#img_').attr('src','');
            }
            // console.log('ss data.country '+data.country);
            $("#country-dropdown").val(data.country).change();
            $("#state-dropdown").val(data.state).change();
            $("#city-dropdown").val(data.city).change();
            $('#state').val(data.state);
            $('#addModal').modal('show');
        })
        $('#addModal').modal('show');
    };


    this.delUser = function (user_id) {
        console.log('ss user_id '+user_id );
        var result = confirm("Do you really want to delete this user ?");
        if (result) {
            $.get('user/' + user_id + '/delete', function (data) {
//                $('#userTable tbody #' + user_id).remove();
                user.createList();
            });
        }
    };



    this.addModalClose = function () {
    };
    
    this.createList = function(){
        
        var html = '';
        
        var i=1;
        $.get('user_list_data', function (data) {
//                var resp = JSON.parse(data);
                $.each(data['users_data'] ,function(ind,val){
                    html += '<tr>';
                    html += '<td>';
                    html += i;
                    html += '</td>';
                    html += '<td>';
                    html += val.name;
                    html += '</td>';
                    html += '<td>';
                    html += val.email;
                    html += '</td>';
                    html += '<td>';
                    html += val.phone_number;
                    html += '</td>';
                    
                    html += '<td>';
                    if(val.path){
                        var path_ = val.path.replace('public', '/storage');
                        html += '<img  height="150" width="150"  src="'+path_+'" alt="No image found" >';
                    }
                    html += '</td>';
                    
                    html += '<td class="text-center" > ';
                    html += '<a  onclick="user.showModal(1,'+"'"+val.id+"'"+')"  class="btn btn-primary btnEdit">Edit</a>';
                    html += '<a  onclick="user.delUser('+"'"+val.id+"'"+')"  class="btn btn-danger btnDelete">Delete</a>';
                    
                    html += '</td>';
                    
                    
                    html += '</tr>';
                    
                    i++;
                });
            $('#body_table').html(html);
        });
        
//        html += '<tr>';
//        html += '<tr>';
        
        
        
    };

    this.resetValues = function () {
        //------ reseting the values 
        $('#name').val('');
        $('#email').val('');
        $('#phone_number').val('');


        //------ reseting the values 
    };

    this.saveData = function () { // user.saveData();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $("#addUser").validate({
            rules: {
                name: "required",
                email: "required",
                phone_number: "required"
            },
            dataType: 'html',
            messages: {
            },
            submitHandler: function (form) {
                var form_action = $("#addUser").attr("action");
                $.ajax({
                    type: 'POST',
                    url: form_action,
                    data: new FormData($("#addUser")[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',

                    success: function (data) {

                        if (data.id) { // data is inserted successfully.
                            //setTimeout(function(){ alert("Hello"); }, 3000);
                            $('#addModal').modal('hide');
                            user.resetValues();
                            if (user.is_edit == 1) {
                                alert('User updated successfully.');
                            } else {
                                alert('User added successfully.');
                            }
                            user.createList();
//                            location.reload();
                        } else {
                            alert('Unable to add the user.');
                        }

                    },
                    error: function (resp) {
                        if (resp.responseJSON.errors) {
                            var text_err = '';
                            $.each(resp.responseJSON.errors, function (key, value) {
                                text_err += value;
                                text_err += '\n';
                            });
                            alert(text_err);
                        }
                        //                              console.log(data);
                    }
                });
            }
        });
    };

};

user.init();