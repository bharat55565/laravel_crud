<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    
    <link rel="stylesheet" href="{{ URL::asset('css/bs.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/datatable.css') }}">
    <!--<link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">-->
    <script src="{{ URL::asset('js/helper/jquery.js') }}"></script>
    <script src="{{ URL::asset('js/helper/bs.js') }}"></script>
    <script src="{{ URL::asset('js/helper/validate.js') }}"></script>
    <!--<script src="{{ URL::asset('js/datatable.js') }}"></script>-->
    
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
   
   

</head>
<body>
<div class="container">
    @yield('content')
</div>
   
</body>
</html>