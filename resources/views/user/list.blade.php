@extends('layouts.app')
@section('content')




<div class="row">
    <div class="col-lg-10">
        <h2>User CRUD </h2>
    </div>
    <div class="col-lg-1">
        <a class="btn btn-success" href="#" data-toggle="modal" onclick="user.showModal();">Add</a>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered" id="userTable">
    <thead>
        <tr>
            <th>S.No.</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>File</th>
            <th class="text-center" width="280px">Action</th>
        </tr>
    </thead>	
    <tbody id="body_table">

     

    </tbody>
</table>


<!-- Add User Modal -->
<div id="addModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">

        <!-- User Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="user.addModalClose();" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="m_title">Add New User</h4>
            </div>
            <div class="modal-body">
                
<!--<form id="addUser" method="POST"  action="javascript:void(0)" enctype="multipart/form-data">-->
                <form id="addUser" name="addUser" accept-charset="utf-8"  enctype="multipart/form-data"  action="{{ route('user.store') }}" method="POST">
                    <input type="hidden" id="_hidden" name="_hidden" value="{{csrf_token()}}">

                    @csrf
                    <input type="hidden" name="id" id="id"/>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="phone_number">Phone Number:</label>
                        <input type="number" class="form-control" id="phone_number" placeholder="Enter Phone Number" name="phone_number">
                    </div>
                    <div class="form-group">
                        <label for="phone_number">Upload File :</label>
                        <input type="file" class="form-control" id="file"  name="file">
                    </div>
                    <input type="hidden" class="form-control" id="already_uploded_path" name="already_uploded_path" value="" >
                    <div class="form-group" id="img_div">
                        <label for="phone_number">Already Uplaoded File:</label>
                        <img src=""  height="150" width="150"  id="img_" alt="No image is uploaded" />
                    </div>

                    <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                    <button type="submit"  class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="user.addModalClose();" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>	


 <script src="{{ URL::asset('js/module/user.js') }}"></script>
 

@endsection