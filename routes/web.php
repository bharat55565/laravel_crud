<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MyController;
use App\Http\Controllers\UserAuth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
});




Route::post('user_add','App\Http\Controllers\UserController@store')->name('user.store');
Route::get('user/{id}/edit', 'App\Http\Controllers\UserController@edit')->name('user.edit');
Route::post('user_update', 'App\Http\Controllers\UserController@update')->name('user.update');
Route::get('user/{id}/delete', 'App\Http\Controllers\UserController@destroy')->name('user.delete');
 

Route::get('user','App\Http\Controllers\UserController@index')->name('user') ;
Route::get('user_list_data','App\Http\Controllers\UserController@userList')->name('user_list_data') ;