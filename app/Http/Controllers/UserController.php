<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Models\File;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['users_data'] = User::all('id', 'name', 'email',  'phone_number','file_name','path');
        return view('user.list', $data);
    }
    
    public function userList() {

        $data['users_data'] = User::all('id', 'name', 'email',  'phone_number','file_name','path');
         return Response::json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //--------- file uploading strt
        $insert = [];
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $path = $file->store('public/files');
            $name = $file->getClientOriginalName();

            $insert['name'] = $name;
            $insert['path'] = $path;
        }
        //--------- file uploading end


        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'phone_number' => 'required|unique:users',
        ]);


        $user = new User([
            'name' => $request->post('name'),
            'email' => $request->post('email'),
            'phone_number' => $request->post('phone_number'),
            'file_name' => $insert['name'],
            'path' => $insert['path'],
        ]);

        $user->save();
        return Response::json($user);
    }

    public function show(User $user) {
        //
    }

    public function edit($id) {
        $where = array('id' => $id);
        $user = User::where($where)->first();
        
        return Response::json($user);
    }

    public function update(Request $request) {

          //--------- file uploading strt
        $insert = [];
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $path = $file->store('public/files');
            $name = $file->getClientOriginalName();

            $insert['name'] = $name;
            $insert['path'] = $path;
        }
        //--------- file uploading end
        
        
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $request->post('id'),
            'phone_number' => 'required|unique:users,phone_number,' . $request->post('id'),
        ]);


        //
        $user = User::find($request->post('id'));
        $user->name = $request->post('name');
        $user->email = $request->post('email');
        $user->phone_number = $request->post('phone_number');
        
        if(isset($insert['path'])){
            $user->path  = $insert['path'];
        }
        
        
        $user->update();
        return Response::json($user);
    }

    public function destroy($id) {
        $user = User::where('id', $id)->delete();
        return Response::json($user);
    }


}

?>